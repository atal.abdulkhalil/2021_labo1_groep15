package tictactoeGame;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class TicTacToeGameTest {
    @BeforeEach
    void setUp(){

    }
    @AfterEach
    void tearDown(){

    }

    @Test
    public void testRange() throws IOException {
        int[] numbers = {40, 86, 75, 76, 101};
        String[] words = {"Tic", "Tac", "Toe"};
        int[] numberParams = {3, 5 , 7};

        TicTacToeGame game = new TicTacToeGame(numbers, words, numberParams, 0, 100);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> game.checkRange());
        assertEquals("Verkeerde getallen", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsFortestResultset")
    public void testResultset(int[] numbers, String[] wordParams, int[]numberParams, String expected){

        TicTacToeGame game = new TicTacToeGame(numbers, wordParams, numberParams, 0, 100);
        String result = "";
//        for(int i = 0; i < numberParams.length; i++ ){
//            result += game.result(numberParams[i], wordParams[i], i);
//        }

        for(int i = 0; i < numbers.length; i++ ){
            int currentNumber = numbers[i];
            String tempResult = "";
            for(int j = 0; j < numberParams.length; j++){
                tempResult += game.result(numberParams[j], wordParams[j], currentNumber);
            }

            //checken of beide "" returnen
            if(tempResult.equals("")){
                result += Integer.toString(currentNumber) + " ";
            }else{
                result += tempResult;
            }
        }
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideArgumentsFortestResultset(){
        return Stream.of(
                Arguments.of(new int[]{3}, new String[]{"Frits ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Frits Frits "),
                Arguments.of(new int[]{13}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tic "),
                Arguments.of(new int[]{33}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tic Tic Tic "),
                Arguments.of(new int[]{5}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tac Tac "),
                Arguments.of(new int[]{15}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tic Tac Tac "),
                Arguments.of(new int[]{25}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tac Tac "),
                Arguments.of(new int[]{7}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Toe Toe "),
                Arguments.of(new int[]{17}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Toe "),
                Arguments.of(new int[]{21}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Tic Toe "),
                Arguments.of(new int[]{14}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "Toe "),
                Arguments.of(new int[]{14}, new String[]{"Tic ", "Tac ", "Frits "}, new int[]{3, 5, 7}, "Frits "),
                Arguments.of(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, new String[]{"Tic ", "Tac ", "Toe "}, new int[]{3, 5, 7}, "1 2 Tic Tic 4 Tac Tac Tic Toe Toe 8 Tic Tac 11 Tic Tic Toe Tic Tac Tac ")
        );
    }
}