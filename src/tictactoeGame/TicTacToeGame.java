package tictactoeGame;

import java.io.IOException;

public class TicTacToeGame {
    private int[] numbers;
    private int minRange;
    private int maxRange;

    public TicTacToeGame(int[] numbers, String[] wordParams, int[] numberParams, int minRange, int maxRange) {
        this.numbers = numbers;
        this.minRange = minRange;
        this.maxRange = maxRange;

        String result = "";

//        for(int i = 0; i < numberParams.length; i++ ){
//            result += result(numberParams[i], wordParams[i]);
//        }

        for(int i = 0; i < numbers.length; i++ ){
            int currentNumber = numbers[i];
            String tempResult = "";
            for(int j = 0; j < numberParams.length; j++){
                tempResult += result(numberParams[j], wordParams[j], currentNumber);
            }

            //checken of beide "" returnen
            if(tempResult.equals("")){
                result += Integer.toString(currentNumber);
            }else{
                result += tempResult;
            }
        }
    }

    public String result(int number, String word, int currentNumber) {
        String result = "";

        int newNum = currentNumber;
        //loop over alle getallen van een getal
        while(newNum > 0){
            int lastDigit = newNum % 10;
            result += hasNumber(lastDigit, number, word);
            newNum /= 10;
        }
        result += isDivisable(currentNumber, number, word);

        return result;
    }

//    public String result(int number, String word) {
//        String result = "";
//        int counter = 0;
//        //loop over alle getallen in de rij
//        for (int num: numbers
//        ) {
//            String tempResult = "";
//
//            int newNum = num;
//            //loop over alle getallen van een getal
//            while(newNum > 0){
//                int lastDigit = newNum % 10;
//                tempResult += hasNumber(lastDigit, number, word);
//                newNum /= 10;
//            }
//            tempResult += isDivisable(num, number, word);
//
//            //checken of beide "" returnen
//            if(tempResult.equals("")){
//                tempResult += Integer.toString(counter);
//            }
//            result += tempResult;
//            counter++;
//        }
//        return result;
//    }

    private String hasNumber(int i, int i2, String s) {
        if (i == i2) {
            return s;
        }
        return "";
    }

    private String isDivisable(int i, int i2, String s){
        if (i % i2 == 0) {
            return s;
        }
        return "";
    }


    public void checkRange() throws IOException {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= minRange || numbers[i] > maxRange) {
                throw new IllegalArgumentException("Verkeerde getallen");
            }
        }
    }
}
